Vue.component('demo', {
    data() {
        return {
            text: 'Hola mundo!, desde un componente.'
        }
    },
    template: '<div id="comp-demo"><h1>{{ text }}</h1></div>'
})